%Document Setup.
\documentclass{article}
\usepackage{graphicx}   %Allows import of images.
\usepackage{float}
\usepackage{siunitx}    %SI Units formatting.
\usepackage{vhistory}   %Revision History.
\usepackage[version=4]{mhchem}  %Chemical Equations.
\usepackage{amsmath}    %Mathematical alignments.
\usepackage{amssymb}    %Mathematical symbols e.g. therefore.
\usepackage[parfill]{parskip}   %Blank lines between paragraphs.

\numberwithin{equation}{section}    %Number equations based on their section.

%Document Headings.
\begin{document}
\title{Preparation of Copper(II) Sulphate}
\author{Sam White 13A}
\date{\vhCurrentDate\\\vhCurrentVersion}
\maketitle

%Version History Table.
\begin{versionhistory}
    \vhEntry{1.0.0}{10.10.2016}{Sam White}{Initial Version}
\end{versionhistory}

\section{Sequential Method}
\begin{enumerate}
    \item Heat \SI{25}{\centi\metre\cubed} of \SI{1.0}{\mole\per\deci\metre\cubed} sulphuric acid, in a \SI{100}{\centi\metre\cubed} beaker, to just under its boiling temperature.
    \item Add copper carbonate on spatula at a time, stiring with a glass rod, until no further reaction occurs.
    \item Filter the mixture removing the excess copper carbonate into an evaporating basin. Fluted filter paper may be used.
    \item Gently simmer the mixture in the evapourating basin until the volume is reduced by approximatly half to produce a super-saturated solution. Stop heating when crytals immediatly form when a cool glass rod is dipped in the solution and then removed.
    \item Leave the solution to slowly cool and the crystals to form as the water evapourates.
    \item After 24 hour drain the excess solution and dry the rombic/parroleogramic crystals by dabbing with absorbant paper.
    \item Transfer the crystals to a weighed, stoppered container and record the mass of the crystals produced.
\end{enumerate}

\subsection{Diagram}
\begin{figure}[H]
    \centering
    \includegraphics[width=1\textwidth]{CuSO4-Heating.jpg}
    \caption{Heating the Copper Sulphate Solution \label{fig:cuso4-heating}}
\end{figure}

\subsection{Reasons for Method}
\begin{itemize}
    \item The sulphuric acid is initally heated to increase the rate of reaction between the sulphuric acid and the copper carbonate.
    \item The copper carbonate is added in excess as it is easier to separate the unreacted substance from the solution (since it is insoluable and forms a suspension) than if the sulphuric acid was added in excess hence reducing contamination of the product.
    \item The filtration removes the excess copper carbonate from the solution. Fluted filter paper increases the speed of filtration by increasing the surface area of the filter paper.
    \item The solution is is super-saturated to increase the speed at which the crstalisation occurs.
    \item The solution should not be heated to dryness as otherwise anhydrous copper sulphate (which is a white powder) would form instead of hydrated copper sulphate crystals. Hence the period of leaving the water to slowly evapurate to allow the crystals to form. 
\end{itemize}

\subsection{Uncertainties in any Measurements}
A \SI{25}{\centi\metre\cubed} measuring cyclinder was used to measure the volume of sulphuric acid added. This has an associated uncertainty of \SI{+-0.5}{\centi\metre\cubed}, hence introducing an uncertainty of $\frac{\SI{+-0.5}{\centi\metre\cubed}}{\SI{25.0}{\centi\metre\cubed}} \times \SI{100}{\percent} = \SI{+-2}{\percent}$.

The mass of the crystals was measured using a mass balance with an uncertainty of \SI{+-0.001}{\gram}. Due to the double measurement overall this becomes $\SI{+-0.001}{\gram} \times 2 = \SI{+-0.002}{\gram}$, hence the percentage uncertainty is $\frac{\SI{+-0.002}{\gram}}{\SI{2.987}{\gram}} \times \SI{100}{\percent} = \SI{+-0.07}{\percent}$ (1sf.).

\section{Results and Observations}
\begin{tabular}{|c|c|}
    \hline
    Mass of Container + Copper(II) Sulphate crystals & \SI{24.551}{\gram} \\
    Mass of Empty Stoppered Container & \SI{21.564}{\gram} \\
    \hline
    Mass of Copper(II) Sulphate Crystals & \SI{2.987}{\gram} \\
    \hline
\end{tabular}

Rombic/parroleogramic dark blue crystals form.

\subsection{Processed Results}
N/A.

\subsection{Calculations}
The chemical equation for the initial reaction between the Copper Carbonate and the Sulphuric acid can be seen in \ref{eq:CuCO3+H2SO4}.
\begin{equation} \label{eq:CuCO3+H2SO4}
    \ce{CuCO3(s) + H2SO4(aq) -> CuSO4(aq) + H2O(l) + CO2(g)}
\end{equation}
The \ce{CuSO4} produced will hence react with any present water as shown in \ref{eq:CuSO4+H2O}.
\begin{equation} \label{eq:CuSO4+H2O}
    \ce{CuSO4(aq) + 5H2O(l) -> CuSO4.5H2O(aq)}
\end{equation}
Combining \ref{eq:CuCO3+H2SO4} and \ref{eq:CuSO4+H2O} then gives \ref{eq:overall}.
\begin{equation} \label{eq:overall}
    \ce{CuCO3(s) + H2SO4(aq) + 4H20(l) -> CuSO4.5H2O(aq) + CO2(g)}
\end{equation}
This thus allows the calculation of the theoretical yield of \ce{CuSO4.5H2O}:
\begin{displaymath}
    \begin{aligned}
        \text{Concentration of \ce{H2SO4}} &= \SI{1.0}{\mole\per\deci\metre\cubed} \\
        \text{Volume of \ce{H2SO4}} &= \SI{25}{\centi\metre\cubed} = \SI{0.025}{\deci\metre\cubed} \\
        \therefore{}\text{Moles of \ce{H2SO4}} &= 0.025 \times 1.0 = \SI{0.025}{\mole}
    \end{aligned}
\end{displaymath}
There is a 1:1 ratio between the \ce{H2SO4} and \ce{CuSO4.5H2O}, hence \SI{0.025}{\mole} of \ce{CuSO4.5H2O} is produced.
\begin{displaymath}
    \begin{aligned}
        \text{Mass of \ce{CuSO4.5H2O}} &= 63.5+32.1+4(16.0)+5(2(1.0)+16.0) = \SI{249.6}{\gram\per\mole} \\
        \therefore{}\text{Theoretical yield of \ce{CuSO4.5H2O}} &= 249.6\times0.025 = \SI{6.24}{\gram}
    \end{aligned}
\end{displaymath}

Hence percentage yield of \ce{CuSO4.5H2O} = $\frac{2.987}{6.24}\times100\%$ = \SI{48}{\percent} (2sf.).

\subsection{Uncertainty in Final Answer}
Overall percentage uncertainty is $\SI{+-2}{\percent} + \SI{+-0.07}{\percent} = \SI{+-2.07}{\percent}$.

Absolute uncertainty is $\frac{\SI{2.987}{\gram} \times \SI{+-2.07}{\percent}}{100} = \SI{+-0.021}{\gram}$ (2sf.). 

\section{Conclusions Drawn}
The percentage yield of the Copper Sulphate Pentahydrate will always be less than 100\% due to various losses which will occur during the experiemnt such as transfer losses and well as impurities which may be present in the reaction mixtures. 

\section{Evaluation}
\subsection{Systematic Errors}
There will have been transfer losses at various places throughout the experiment such as when the sulphuric acid was transfered to the beaker from the measuring cylinder or when the solution was transfered from the beaker to the evapourating dish. This could be minimised by rinsing the apparatus with distilled water and transfering the rinsings to the new container.

The final copper sulphate pentahydrate solution could be left for longer than 24 hours such that the crystals have a greater time to crystalise and hence more crystals will form from the solution hence a greater yield will be obtained.

\subsection{Uncertainties}
The volume of sulphuric acid was measured with a measuring cylinder which introduced a \SI{+-2}{\percent} uncertainty. A \SI{25}{\centi\metre\cubed} graduated pipette could be used instead which has a lower associated percentage uncertainty, as it is a more precise instrument, hence this would reduce the percentage uncertainty in this measurement. A greater volume of sulphuric acid could also be used to reduce the percentage uncertainty in this measurement. This would also reduce the percentage uncertainty in the mass of \ce{CuSO4.H2O} produced as this would produce a greater mass of the product. Alternativly the percentage uncertainty in the mass of the \ce{CuSO4.H2O} could also be reduced be using a greater concentration of sulphuric acid.

A more precise mass balance could also be used to measure the mass of copper sulphate pentahydrate produced which would further reduce the already quite low percentage uncertainty since it would have a lower associated absolute uncertainty.

\end{document}
